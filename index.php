<!DOCTYPE html>
<html lang="en">
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-P2B4L0Y3VB"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-P2B4L0Y3VB');
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Coconut Yoga - Studio de yoga</title>
    <meta name="description" content="Le studio Coconut Yoga offre des cours variés de yoga et de méditation à Stoneham-et-Tewkesbury, près de Québec.">
    <meta name="author" content="Les Mauvais Garçons">
    <link rel="canonical" href="https://coconutyoga.ca"/>
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="assets/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#F2D5CC">

    <meta property="og:url"                content="https://coconutyoga.ca" />
    <meta property="og:title"              content="Coconut Yoga - Studio de yoga" />
    <meta property="og:description"        content="Le studio Coconut Yoga offre des cours variés de yoga et de méditation à Stoneham-et-Tewkesbury, près de Québec." />
    <meta property="og:image"              content="https://coconutyoga.ca/assets/images/coconut-yoga-ogshare.jpg" />

    <link rel="stylesheet" href="https://use.typekit.net/zeg3qdg.css">
    <link rel="stylesheet" href="assets/css/main.min.css?v=2.3">
</head>
<body>
    <header class="header">
        <div class="header__top container">
            <img class="logo" src="assets/images/Logo-coconut-yoga.svg" alt="Coconut yoga logo.">
            <div class="header__top__rs">
                <a rel="noreferrer" href="https://www.facebook.com/coconutyogastoneham" target="_blank">
                    <span class="rs__text">Facebook</span>
                    <span class="rs__icon">
                        <svg width="10px" height="20px" viewBox="0 0 10 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <title>facebook (8)</title>
                            <desc>Created with Sketch.</desc>
                            <g id="svgFacebook" stroke="none" stroke-width="1" fill="#ECE5DD" fill-rule="evenodd">
                                <g transform="translate(-249.000000, -31.000000)" fill="" fill-rule="nonzero">
                                    <g transform="translate(249.000000, 31.000000)">
                                        <path d="M8.14997416,3.29509044 L9.96165375,3.29509044 L9.96165375,0.139741602 C9.64909561,0.096744186 8.57416021,0 7.3222739,0 C4.71018088,0 2.92082687,1.64299742 2.92082687,4.66273902 L2.92082687,7.44186047 L0.0383462532,7.44186047 L0.0383462532,10.9693023 L2.92082687,10.9693023 L2.92082687,19.8449612 L6.45488372,19.8449612 L6.45488372,10.9701292 L9.22077519,10.9701292 L9.65984496,7.44268734 L6.45405685,7.44268734 L6.45405685,5.01250646 C6.45488372,3.99297158 6.72940568,3.29509044 8.14997416,3.29509044 L8.14997416,3.29509044 Z"></path>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </span>
                </a>
                <a rel="noreferrer" href="https://www.instagram.com/studiococonutyoga/?hl=fr-ca" target="_blank">
                    <span class="rs__text">Instagram</span>
                    <span class="rs__icon">
                        <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <title>instagram (2)</title>
                            <desc>Created with Sketch.</desc>
                            <g id="svgInstagram" stroke="none" stroke-width="1" fill="#ECE5DD" fill-rule="evenodd">
                                <g transform="translate(-279.000000, -31.000000)" fill="" fill-rule="nonzero">
                                    <g transform="translate(279.000000, 31.000000)">
                                        <path d="M10.02282,4.85551657 C7.19251462,4.85551657 4.9011566,7.14936972 4.9011566,9.97717999 C4.9011566,12.8074854 7.19500975,15.0988434 10.02282,15.0988434 C12.8531254,15.0988434 15.1444834,12.8049903 15.1444834,9.97717999 C15.1444834,7.14687459 12.8506303,4.85551657 10.02282,4.85551657 Z M10.02282,13.3015205 C8.18557505,13.3015205 6.69847953,11.8135932 6.69847953,9.97717999 C6.69847953,8.14076673 8.18640676,6.65283951 10.02282,6.65283951 C11.8592333,6.65283951 13.3471608,8.14076673 13.3471608,9.97717999 C13.3479922,11.8135932 11.860065,13.3015205 10.02282,13.3015205 L10.02282,13.3015205 Z" id="Shape"></path>
                                        <path d="M14.1347888,0.0632098765 C12.2983756,-0.0224561404 7.74975958,-0.0182975958 5.91168291,0.0632098765 C4.29650422,0.138895387 2.87178687,0.528966862 1.72319688,1.67755686 C-0.196387264,3.597141 0.0489668616,6.18375569 0.0489668616,9.97717999 C0.0489668616,13.8595971 -0.167277453,16.3863288 1.72319688,18.2768031 C3.65026641,20.2030409 6.27430799,19.9510331 10.02282,19.9510331 C13.868642,19.9510331 15.1960494,19.9535283 16.5558934,19.4270565 C18.4047823,18.7092917 19.8003899,17.0566862 19.9367901,14.0883171 C20.0232878,12.2510721 20.0182976,7.70328785 19.9367901,5.86521118 C19.7721118,2.36122157 17.8916179,0.236205328 14.1347888,0.0632098765 Z M17.0416114,17.0067836 C15.7832359,18.2651592 14.0374789,18.1528785 9.99870045,18.1528785 C5.84015595,18.1528785 4.1725796,18.214425 2.95578947,16.994308 C1.55435997,15.5995322 1.80803119,13.3597401 1.80803119,9.96387264 C1.80803119,5.36868096 1.33645224,2.05931124 5.9482781,1.82310591 C7.00787524,1.78567901 7.31976608,1.77320338 9.98705653,1.77320338 L10.0244834,1.79815465 C14.4566602,1.79815465 17.9340351,1.33406108 18.142794,5.94505523 C18.1902014,6.99716699 18.2010136,7.31321637 18.2010136,9.97634828 C18.2001819,14.0866537 18.2783626,15.7642105 17.0416114,17.0067836 Z"></path>
                                        <circle id="Oval" cx="15.3474204" cy="4.65341131" r="1.19682911"></circle>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </span>
                </a>
            </div>
        </div>
        <div class="header__contenu container">
            <div class="header__contenu__left">
                <h1>Maintenant ouvert</h1>
                <p>Situé à Stoneham, Coconut Yoga est un espace dédié au partage, aux ateliers de développement et à la pratique du yoga pour tous les niveaux.</p>
                <a class="btn" href="https://coconutyoga.fliipapp.com/horaire" target="_blank">Voir l'horaire</a>
                <a class="btn" href="https://coconutyoga.fliipapp.com/user/register/buy_membership#2564" target="_blank">Réserver un cours</a>
            </div>
            <div class="header__contenu__right">
                <h2>Infolettre</h2>
                <p>Reste à l'affût de nos nouvelles et promotions en t'inscrivant à notre infolettre.</p>

                <form action="https://coconutyoga.us7.list-manage.com/subscribe/post?u=f1269a72d376e6d8da5776cd9&amp;id=5760aecdd7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="form validate">
                    <input type="hidden" name="u" value="f1269a72d376e6d8da5776cd9">
                    <input type="hidden" name="id" value="5760aecdd7">
                    <div class="form__row1">
                        <div class="form__row1__input--text mc-field-group">
                            <label for="mce-FNAME">Prénom  <span class="asterisk">*</span></label>
                            <input type="text" value="" name="FNAME" class="required" id="mce-FNAME" placeholder="Prénom" required>
                        </div>
                        <div class="form__row1__input--text mc-field-group">
                            <label for="mce-LNAME">Nom  <span class="asterisk">*</span></label>
                            <input type="text" value="" name="LNAME" class="required" id="mce-LNAME" placeholder="Nom" required>
                        </div>
                    </div>
                    <div class="form__row2">
                        <div class="form__row2__input--text mc-field-group">
                            <label for="mce-EMAIL">Courriel  <span class="asterisk">*</span></label>
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Courriel" required>
                        </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>    
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_f1269a72d376e6d8da5776cd9_5760aecdd7" tabindex="-1" value=""></div>
                        <input type="submit" value="M'inscrire" name="subscribe" id="mc-embedded-subscribe" class="button">
                    </div>
                </form>
            </div>
        </div>
    </header>
    <footer class="contact">
        <div class="contact__links container">
            <p><a class="links" rel="noreferrer" href="https://goo.gl/maps/6r3F5JuLZTkqMGre7" target="_blank">4 route Tewkesbury, suite 105, Stoneham-Tewkesbury (Québec), G3C 2K6</a></p>
            <div class="contact__links__duo">
                <p><a class="links" rel="noreferrer" href="mailto:allo@coconutyoga.ca">allo@coconutyoga.ca</a></p>
                <p><a class="links" rel="noreferrer" href="tel:418-928-8252">418-928-8252</a></p>
            </div>
        </div>
        <div class="container">
            <small>Réalisé par <a rel="noreferrer" href="https://lesmauvaisgarcons.ca/" target="_blank">Les Mauvais Garçons</a></small>
        </div>
    </footer>

    <?php if($_GET['subscribe'] == 'true'){ ?>
        <div class="popup">
            <svg width="30px" height="37px" viewBox="0 0 30 37" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <!-- Generator: Sketch 57.1 (83088) - https://sketch.com -->
                <title>Group 4</title>
                <desc>Created with Sketch.</desc>
                <g id="Maquettes" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Coconut-yoga---Accueil---V3" transform="translate(-498.000000, -827.000000)" fill="#ECE5DD">
                        <g id="Group-4" transform="translate(498.000000, 827.000000)">
                            <g id="SCY-Symbole-Noir">
                                <path d="M23.2951196,7.76216147 L23.4408232,7.86042995 C27.3848724,10.5250066 29.835,15.3258606 29.835,20.3895191 C29.835,25.3923616 27.3288707,30.1932864 23.294624,32.9185374 C20.9804297,34.4822664 15.3856401,36.6310044 15.1486063,36.7217682 L14.9999292,36.7786195 L14.8513229,36.7217682 C14.6142184,36.6310044 9.01964107,34.4822664 6.70530517,32.9185374 C2.67112934,30.1932864 0.165,25.3923616 0.165,20.3895191 C0.165,15.3258606 2.61512764,10.5250066 6.55917683,7.86042995 L6.70480958,7.76216147 L6.87677941,7.79777317 C10.1005235,8.46802365 12.9218483,10.3240801 14.9999292,13.0647653 C17.0780101,10.3240801 19.8993349,8.46802365 23.1232206,7.79777317 L23.2951196,7.76216147 Z M23.120247,8.6496929 C20.0900664,9.32539487 17.4446053,11.1301931 15.5088296,13.7755834 C16.1533802,14.7298071 16.7134681,15.7773434 17.1772698,16.9083512 C18.0885895,19.1305073 18.3346147,21.6732396 17.8886543,24.2614245 C17.4872969,26.5905573 16.5475869,28.9143094 15.1709787,30.9819121 L14.8246318,31.5019988 L14.4782142,30.9819121 C11.6430837,26.7238711 11.0087281,21.3312115 12.8227302,16.9083512 C13.2864611,15.7773434 13.846549,14.7298071 14.4911704,13.7755834 C12.5553239,11.1301931 9.90979204,9.32539487 6.87975296,8.6496929 C3.24700527,11.1807433 0.997096111,15.6613752 0.997096111,20.3895191 C0.997096111,25.1197161 3.3629026,29.6564205 7.17108925,32.2290297 C8.35903607,33.0316327 10.5213158,34.0243973 12.2953871,34.7818034 C9.69710376,32.5887907 7.71778604,30.1800814 6.39088853,27.595048 C5.00868718,24.9021519 4.32894969,22.016542 4.37036688,19.0187163 C4.4408115,13.9210745 6.54912343,10.3165047 6.63882527,10.165562 L7.35403287,10.5906369 C7.32436825,10.6409039 5.25910188,14.1932952 5.2018966,19.0672842 C5.16826726,21.9225923 5.82117208,24.6712777 7.14248661,27.2370194 C8.69508897,30.2521308 11.1821175,33.0266365 14.541203,35.4932849 L15.004,35.8276429 L15.2211435,35.6803468 C18.4948767,33.4147404 20.9327792,30.7377554 22.4715901,27.7166801 C23.7563726,25.194621 24.4231539,22.4240589 24.4533849,19.4818809 C24.5051387,14.4456214 22.6489407,10.5997699 22.6300374,10.5613971 L23.3769628,10.1945187 L23.397233,10.2365427 C23.616038,10.6961919 25.3290376,14.4569862 25.2857643,19.4560394 C25.2590732,22.5328763 24.5651052,25.4327168 23.222905,28.0750626 C21.9793197,30.5232578 20.1754093,32.7503943 17.841802,34.7237247 C19.5881187,33.9739961 21.6712026,33.0112095 22.8288399,32.2290297 C26.6371682,29.6564205 29.0029039,25.1197161 29.0029039,20.3895191 C29.0029039,15.6613752 26.7529947,11.1807433 23.120247,8.6496929 Z M14.9999292,14.5158537 C14.4624261,15.3487994 13.990341,16.2542428 13.5925944,17.224113 C11.9579959,21.209367 12.4377982,26.0391777 14.8256938,29.9777046 C15.9496926,28.1469232 16.7216807,26.1345434 17.0686647,24.1201813 C17.4940936,21.651292 17.2715735,19.3311506 16.4074056,17.224113 C16.009659,16.2542428 15.5375739,15.3487994 14.9999292,14.5158537 Z M14.9999292,0.314642857 C16.9378289,0.314642857 18.5143724,1.89125719 18.5143724,3.82901526 C18.5143724,5.76684413 16.9378289,7.34338767 14.9999292,7.34338767 C13.0620295,7.34338767 11.485486,5.76684413 11.485486,3.82901526 C11.485486,1.89125719 13.0620295,0.314642857 14.9999292,0.314642857 Z M14.9999292,1.14673897 C13.5208754,1.14673897 12.3175821,2.34989062 12.3175821,3.82901526 C12.3175821,5.30799831 13.5208754,6.51129156 14.9999292,6.51129156 C16.478983,6.51129156 17.6822763,5.30799831 17.6822763,3.82901526 C17.6822763,2.34989062 16.478983,1.14673897 14.9999292,1.14673897 Z" id="Combined-Shape"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
            <p class="popup__titre">Inscription réussie, merci beaucoup!</p>
        </div>
    <?php } ?>
    
</body>
</html>