// Setup des dépendances
var gulp = require("gulp");
var sass = require("gulp-sass");
var rename = require('gulp-rename');
var postcss = require('gulp-postcss');
var autoprefixer = require('gulp-autoprefixer');
var cssnano = require('cssnano');
var browserSync = require("browser-sync").create();

var paths = {

  build: {
    dest: 'assets/build/min/'
  },

  styles: {
    src: 'assets/scss/**/*.scss',
    dest: 'assets/css'
  },
  scripts: {
    src: 'assets/js/**/*.js',
    dest: 'assets/js/min'
  }
};

function style() {
  return (
      gulp
          .src(paths.styles.src)
          // Initialize sourcemaps before compilation starts
          .pipe(sass())
          .on("error", sass.logError)
          // Use postcss with autoprefixer and compress the compiled file using cssnano
          .pipe(postcss([autoprefixer, cssnano]))
          .pipe(rename({
            suffix: ".min"
          }))
          .pipe(gulp.dest(paths.styles.dest))
          .pipe(browserSync.stream())
  );
}

// Expose the task by exporting it
// This allows you to run it from the commandline using
// $ gulp style
function watch(){
  browserSync.init({
    // You can tell browserSync to use this directory and serve it as a mini-server
    
        proxy: "localhost:8888/coconut-yoga"
  });
  // gulp.watch takes in the location of the files to watch for changes
  // and the name of the function we want to run on change
  gulp.watch(paths.styles.src, style)
}
  
exports.style = style;
exports.watch = watch